/*
 * decimal.cpp
 *
 *  Created on: Dec 4, 2014
 *      Author: busygin
 */

#include "decimal.h"


namespace my_order_book {


Decimal operator+(Decimal a, const Decimal& b) { return a += b; }
Decimal operator-(Decimal a, const Decimal& b) { return a -= b; }

template<typename T>
Decimal operator*(Decimal a, const T& mult) { return a *= mult; }

template<typename T>
Decimal operator/(Decimal a, const T& div) { return a /= div; }


}
