/*
 * order.cpp
 *
 *  Created on: Dec 4, 2014
 *      Author: busygin
 */


#include "order.h"
#include <cstdio>


namespace my_order_book {


Order::Order(uint64_t order_id
    ,const std::string& symbol
    ,Side side
    ,uint64_t qty
    ,Decimal price
    ,Type type
    ,TimeInForce time_in_force)
  :
    order_id_(order_id)
    ,symbol_(symbol)
    ,side_(side)
    ,qty_(qty)
    ,price_(price)
    ,type_(type)
    ,time_in_force_(time_in_force)
    ,stop_price_(0.0)
    ,open_qty_(qty)
    ,executed_qty_(0)
    ,avg_executed_price_(0.0)
    ,last_executed_price_(0.0)
    ,last_executed_qty_(0) { }


void Order::set_qty(uint64_t qty) {
  if(qty <= executed_qty_) {
    qty_ = executed_qty_;
    open_qty_ = 0;
  } else {
    qty_ = qty;
    open_qty_ = qty_ - executed_qty_;
  }
}

void Order::fill(Decimal price, uint64_t qty) {
  avg_executed_price_ = (double(price)*qty + avg_executed_price_*executed_qty_) / (qty + executed_qty_);
  open_qty_ -= qty;
  executed_qty_ += qty;
  last_executed_price_ = price;
  last_executed_qty_ = qty;
}

std::string Order::as_string() const {
  static char buf[128];
  if (side_==Side::BUY)
    sprintf(buf, "%5ld %6.2f", open_qty_, double(price_));
  else if (side_==Side::SELL)
    sprintf(buf, "%6.2f %ld", double(price_), open_qty_);
  return std::string(buf);
}

}
