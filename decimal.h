/*
 * decimal.h
 *
 *  Created on: Dec 4, 2014
 *      Author: busygin
 */

#ifndef DECIMAL_H_
#define DECIMAL_H_

#include <stdint.h>
#include <cmath>
#include <cassert>


namespace my_order_book {

constexpr int32_t PRECISION=2;
constexpr int64_t MANT_MULT = std::pow(10,PRECISION);

struct Decimal {
  int64_t mantissa;
  int32_t exponent;

  Decimal(int64_t i_mantissa=0, int32_t i_exponent=0) :
    mantissa(i_mantissa), exponent(i_exponent) { }

  Decimal(double v) : mantissa(static_cast<int64_t>(v*MANT_MULT)), exponent(-PRECISION) { }

  explicit operator double() const { return mantissa * std::pow(10,exponent); }

  bool operator==(const Decimal& d) const {
    return (mantissa==d.mantissa) and (exponent==d.exponent);
  }

  Decimal& operator+=(const Decimal& inc) {
    assert(exponent==inc.exponent);
    mantissa += inc.mantissa;
    return *this;
  }

  Decimal& operator-=(const Decimal& dec) {
    assert(exponent==dec.exponent);
    mantissa -= dec.mantissa;
    return *this;
  }

  template<typename T>
  Decimal& operator*=(const T& mult) {
    mantissa *= mult;
    return *this;
  }

  template<typename T>
  Decimal& operator/=(const T& div) {
    mantissa /= div;
    return *this;
  }

  bool operator<(const Decimal& d) const {
    assert(exponent==d.exponent);
    return mantissa < d.mantissa;
  }

  bool operator<=(const Decimal& d) const {
    assert(exponent==d.exponent);
    return mantissa <= d.mantissa;
  }
};


}


#endif /* DECIMAL_H_ */
