/*
 * order.h
 *
 *  Created on: Dec 4, 2014
 *      Author: busygin
 */

#ifndef ORDER_H_
#define ORDER_H_

#include "decimal.h"
#include <string>


namespace my_order_book {


class Order {
public:
  enum class Side : uint8_t {
    BUY = 1,
    SELL
  };

  enum class Type : uint8_t {
    LIMIT = 1,
    MARKET
  };

  enum class TimeInForce : uint8_t {
    DAY = 1,
    IOC
  };

  enum class Status : uint8_t {
    NEW = 1,
    PARTIALLY_FILLED,
    FILLED,
    CANCELLED,
    REPLACED,
    REJECTED,
    EXPIRED
  };

  enum class ExecutionReport : uint8_t {
    NEW = 1,
    PARTIALL_FILL,
    FILL,
    CANCEL,
    REPLACE,
    REJECT,
    EXPIRE
  };

  Order(uint64_t order_id
      ,const std::string& symbol
      ,Side side
      ,uint64_t qty
      ,Decimal price
      ,Type type
      ,TimeInForce time_in_force);

  uint64_t get_order_id() const { return order_id_; }
  std::string get_symbol() const { return symbol_; }
  Side get_side() const { return side_; }
  uint64_t get_qty() const { return qty_; }
  Decimal get_price() const { return price_; }
  Type get_type() const { return type_; }
  TimeInForce get_time_in_force() const { return time_in_force_; }

  uint64_t get_open_qty() const { return open_qty_; }
  uint64_t get_executed_qty() const { return executed_qty_; }
  double get_avg_executed_price() const { return avg_executed_price_; }
  Decimal get_last_executed_price() const { return last_executed_price_; }
  uint64_t get_last_executed_qty() const { return last_executed_qty_; }
  bool is_open() const { return open_qty_ > 0; }
  void set_stop_price(Decimal stop_price) { stop_price_ = stop_price; }
  void set_price(Decimal price) { price_ = price; }
  void set_qty(uint64_t qty);
  void set_open_qty(uint64_t open_qty) { open_qty_ = open_qty; }

  void fill(Decimal price, uint64_t qty);

  Order *clone() {
    return new Order(*this);
  }

  std::string as_string() const;

private:
  uint64_t order_id_;
  std::string symbol_;
  Side side_;
  uint64_t qty_;
  Decimal price_;
  Type type_;
  TimeInForce time_in_force_;

  Decimal stop_price_;

  uint64_t open_qty_;
  uint64_t executed_qty_;
  double avg_executed_price_;
  Decimal last_executed_price_;
  uint64_t last_executed_qty_;
};


}


#endif /* ORDER_H_ */
