/*
 * main.cpp
 *
 *  Created on: Dec 4, 2014
 *      Author: busygin
 */


#include "decimal.h"
#include "order.h"
#include "order_book.h"

#include <iostream>

using namespace my_order_book;

bool unit_test1() {
  Decimal p(10.89);
  return (p.mantissa==1089) and (p.exponent==-2);
}

bool unit_test2() {
  Decimal p1(11.67);
  Decimal p2(1167, -2);
  return p1==p2;
}

bool unit_test3() {
  Order o(1, "CLF5", Order::Side::BUY, 100, 65.55, Order::Type::LIMIT, Order::TimeInForce::IOC);
  o.fill(65.50, 20);
  o.fill(65.55, 30);
  return o.get_avg_executed_price()==65.53;
}

bool unit_test4() {
  OrderBook book("CLF5");

  Order o1(1, "CLF5", Order::Side::BUY, 100, 65.55, Order::Type::LIMIT, Order::TimeInForce::DAY);
  book.match(o1);
  book.add(o1);

  Order o2(2, "CLF5", Order::Side::BUY, 50, 65.65, Order::Type::LIMIT, Order::TimeInForce::DAY);
  book.match(o2);
  book.add(o2);

  Order o3(3, "CLF5", Order::Side::SELL, 160, Decimal(6610, -2), Order::Type::LIMIT, Order::TimeInForce::DAY);
  book.match(o3);
  book.add(o3);

  std::cout << book.pretty_print() << '\n';

  Order o4(4, "CLF5", Order::Side::SELL, 160, 65.40, Order::Type::LIMIT, Order::TimeInForce::DAY);
  book.match(o4);
  book.add(o4);

  std::cout << book.pretty_print() << '\n';

  return (o4.get_open_qty()==10) and (o4.get_avg_executed_price()>65.58) and (o4.get_avg_executed_price()<65.59);
}


int main() {
  std::cout << unit_test1() << '\n';
  std::cout << unit_test2() << '\n';
  std::cout << unit_test3() << '\n';
  std::cout << unit_test4() << '\n';

  return 0;
}
