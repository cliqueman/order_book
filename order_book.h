/*
 * order_book.h
 *
 *  Created on: Dec 4, 2014
 *      Author: busygin
 */

#ifndef ORDER_BOOK_H_
#define ORDER_BOOK_H_

#include "order.h"
#include <string>
#include <map>


namespace my_order_book {


class OrderBook {
private:
  using Bids = std::multimap<double, Order, std::greater<double>>;
  using Asks = std::multimap<double, Order>;

public:
  explicit OrderBook(const std::string &symbol);

  const std::string &get_symbol() const { return symbol_; }
  void match(Order& new_order);
  void add(Order& new_order);
  bool remove(Order::Side side, uint64_t orderId);

  std::string pretty_print() const;

private:
  void match(Order& new_order, Order& book_order);

  std::string symbol_;
  Bids bids_;
  Asks asks_;
};


}


#endif /* ORDER_BOOK_H_ */
