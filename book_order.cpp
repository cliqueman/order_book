/*
 * book_order.cpp
 *
 *  Created on: Dec 4, 2014
 *      Author: busygin
 */

#include "order_book.h"
#include <algorithm>


namespace my_order_book {


OrderBook::OrderBook(const std::string& symbol) :
    symbol_(symbol) { }


void OrderBook::match(Order& new_order) {
  if (new_order.get_side()==Order::Side::BUY) {
    for(Asks::iterator it=asks_.begin(); it!=asks_.end();) {
      Order& ask = it->second;
      if (new_order.get_price() < ask.get_price())
        return;
      match(new_order, ask);
      if (!ask.is_open())
        it = asks_.erase(it);
      else
        ++it;
      if (!new_order.is_open())
        return;
    }
  } else if (new_order.get_side()==Order::Side::SELL) {
    for(Bids::iterator it=bids_.begin(); it!=bids_.end();) {
      Order& bid = it->second;
      if (bid.get_price() < new_order.get_price())
        return;
      match(new_order, bid);
      if (!bid.is_open())
        it = bids_.erase(it);
      else
        ++it;
      if (!new_order.is_open())
        return;
    }
  }
}

void OrderBook::match(Order& new_order, Order& book_order) {
  Decimal fill_price = book_order.get_price();
  uint64_t fill_qty = std::min(new_order.get_open_qty(), book_order.get_open_qty());
  new_order.fill(fill_price, fill_qty);
  book_order.fill(fill_price, fill_qty);
}

void OrderBook::add(Order& new_order) {
  if(new_order.is_open()) {
    if(new_order.get_side() == Order::Side::BUY) {
      bids_.insert(Bids::value_type(double(new_order.get_price()), new_order));
    } else {
      asks_.insert(Asks::value_type(double(new_order.get_price()), new_order));
    }
  }
}

bool OrderBook::remove(Order::Side side, uint64_t order_id) {
  if (side==Order::Side::BUY) {
    for (Bids::iterator it=bids_.begin(); it!=bids_.end(); ++it)
      if (it->second.get_order_id()==order_id) {
        bids_.erase(it);
        return true;
      }
  } else if (side==Order::Side::SELL) {
    for (Asks::iterator it=asks_.begin(); it!=asks_.end(); ++it)
      if (it->second.get_order_id()==order_id) {
        asks_.erase(it);
        return true;
      }
  }
  return false;
}

std::string OrderBook::pretty_print() const {
  std::string output;
  Bids::const_iterator itb = bids_.begin();
  Asks::const_iterator ita = asks_.begin();
  while ((itb!=bids_.end()) or (ita!=asks_.end())) {
    if (itb!=bids_.end()) {
      output += itb->second.as_string();
      ++itb;
    } else
      output.resize(output.size()+12, ' ');
    output.resize(output.size()+10, ' ');
    if (ita!=asks_.end()) {
      output += ita->second.as_string();
      ++ita;
    }
    output.push_back('\n');
  }
  return output;
}

}
